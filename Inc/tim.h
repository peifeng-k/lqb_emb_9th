#ifndef __TIM_H
#define __TIM_H

#include "stm32f10x.h"
#include "stm32f10x_rcc.h"
#include "stm32f10x_tim.h"
#include "stm32f10x_gpio.h"
#include "misc.h"

void TIM4_Config(void);
void TIM3_Config(void);
#endif

