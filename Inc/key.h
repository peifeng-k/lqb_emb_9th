#ifndef __KEY_H
#define __KEY_H

#include "stm32f10x.h"
#include "stm32f10x_gpio.h"
#include "stm32f10x_rcc.h"

void key_gpio_config(void);
void key_scan(void);
uint8_t key_drive(void);



#endif
