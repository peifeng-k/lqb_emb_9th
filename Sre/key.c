#include "key.h"

#define KEY1      GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_0)
#define KEY2      GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_8)
#define KEY3      GPIO_ReadInputDataBit(GPIOB, GPIO_Pin_1)
#define KEY4      GPIO_ReadInputDataBit(GPIOB, GPIO_Pin_2)

uint8_t keySta[] = {1,1,1,1};
uint8_t SkeyMap[] = {0x11,0x12,0x13,0x14};
uint8_t LkeyMap[] = {0x21,0x22,0x23,0x24};
uint16_t keyCount[4]; //长短按键计时，为什么是数组，因为有四个按键

void key_gpio_config(void)
{
	GPIO_InitTypeDef gpio_structure;
	
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA | RCC_APB2Periph_GPIOB, ENABLE);
	
	gpio_structure.GPIO_Mode = GPIO_Mode_IPU;
	gpio_structure.GPIO_Speed = GPIO_Speed_50MHz;
	gpio_structure.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_8;
	GPIO_Init(GPIOA, &gpio_structure);
	
	gpio_structure.GPIO_Pin = GPIO_Pin_1 | GPIO_Pin_2;
	GPIO_Init(GPIOB, &gpio_structure);
	
}

void key_scan(void)
{
	static uint8_t buff[] = {0xff, 0xff,0xff,0xff};
	uint8_t i;
	buff[0] = (buff[0] << 1) | KEY1;
	buff[1] = (buff[1] << 1) | KEY2;
	buff[2] = (buff[2] << 1) | KEY3;
	buff[3] = (buff[3] << 1) | KEY4;
	for(i = 0; i < 4; i ++)
	{
		if((buff[i] & 0xFf) == 0xFf)
		{
			keySta[i] = 1;
			keyCount[i] = 0;//如果弹起就清零
		}
		else if((buff[i] & 0xFf) == 0x00)
		{
			keySta[i] = 0;
			keyCount[i] += 1;//如果一直按着就一直加
			if(keyCount[i] > 1000)
			{
				keyCount[i] = 1000;
			}
		}	
	}
}

uint8_t key_drive(void)
{
	uint8_t i, res = 0;
	static uint8_t backup[] = {1,1,1,1};
	uint16_t timeTr[] = {800,800,800,800};
	for(i = 0; i < 4; i ++)
	{	
		if(backup[i] != keySta[i])
		{
			if(backup[i] != 0)
			{
				res = SkeyMap[i];
				if(keyCount[i] > 0)
				{
					if(keyCount[i] > timeTr[i])
					{
						res = LkeyMap[i];
					}
				}
			}
			backup[i] = keySta[i];
		}//以上的是短按键实现
		if(keyCount[i] > 1)
		{
			if(keyCount[i] > timeTr[i])
			{
				res = LkeyMap[i];
			}
		}//这个是长时间按键的实现
	}
	return res;
}

//uint8_t key_drive(void)
//{
//	uint8_t i, res = 0;
//	for(i = 0; i < 4; i ++)
//	{
//		if(keySta[i] == 1)
//		{
//			if(keyCount[i] > 0 && keyCount[i] < 800)
//			{
//				keyCount[i]  = 0;
//				res = SkeyMap[i];
//			}
//			else if(keyCount[i] > 800)
//			{
//				keyCount[i]  = 0;
//				res = LkeyMap[i];			
//			}
//		}
//	}
//	return res;
//}

