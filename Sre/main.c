#include "stm32f10x.h"
#include <stdio.h>
#include "lcd.h"
#include "key.h"
#include "tim.h"
#include "i2c.h"

#define STANNDBY   0
#define SETTING    1
#define RUNNING    2
#define PAUSE      3

#define LED_ENANLE()  	GPIO_SetBits(GPIOD, GPIO_Pin_2);\
						GPIO_Write(GPIOC, 0XFE00);\
						GPIO_ResetBits(GPIOD, GPIO_Pin_2)
#define LED_DISANLE()   GPIO_SetBits(GPIOD, GPIO_Pin_2);\
						GPIO_Write(GPIOC, 0XFF00);\
						GPIO_ResetBits(GPIOD, GPIO_Pin_2)

u32 TimingDelay = 0;
uint16_t CNT = 0; //定时器计数变量
uint16_t LED_CNT = 0; //LED计数变量
uint8_t start_falg = 0; //开始计数的标志
uint8_t stanndby_Select_count = 0; //存储位置选择标志
uint8_t setting_Select_count = 1; //设置高亮位置选择标志
uint8_t led_flag = 0;
struct time_struct{
	char No;
	int Hour;
	int Min;
	int Sec;
};

struct time_struct play_time[5];

void display(struct time_struct display_time, uint8_t status, uint8_t pos); //显示函数
void set_display(uint8_t Line, uint8_t *ptr,uint8_t pos); //设置时显示的界面
void Delay_Ms(u32 nTime); //延时函数
void LED_gpio_cogfig(void); //LED配置函数
void save_data(struct time_struct save_time);
void read_data(struct time_struct *save_time, uint8_t No);


uint8_t key_val; //键值
//Main Body
int main(void)
{
	uint8_t UI_flag = 1, i; //UI_flag是各个显示界面的标志
	SysTick_Config(SystemCoreClock/1000);
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_0);
	TIM4_Config(); //定时器4初始化用于提供1ms的时基
	TIM3_Config(); //定时器3初始化用于产生pwm
	key_gpio_config(); //按键初始化
	STM3210B_LCD_Init(); //LCD初始化
	LCD_Clear(Blue);
	LCD_SetBackColor(Blue);
	LCD_SetTextColor(White);
	LED_gpio_cogfig(); //LED灯初始化
	i2c_init();

//	play_time[0].No = '3';
//	play_time[0].Hour = 0;
//	play_time[0].Min = 0;
//	play_time[0].Sec = 0;	
//	save_data(play_time[0]);
	for(i = 0; i < 5; i ++)
	{
		read_data(&play_time[i], i + 1);
	}
	while(1)
	{
		while(UI_flag == 1)
		{
			key_val = key_drive();
			if(key_val == 0x11)
			{
				stanndby_Select_count = stanndby_Select_count + 1;
				if(stanndby_Select_count >= 5)
				{
					stanndby_Select_count = 0;
				}
				
			}
			display(play_time[stanndby_Select_count], STANNDBY, 0);
			if(key_val == 0x12)
			{
				UI_flag = 2;
			}
			else if(key_val == 0x14)
			{
				UI_flag = 3;
				start_falg = 1;
			}
		}
		while(UI_flag == 2)
		{
			key_val = key_drive();
			if(key_val == 0x12)
			{
				setting_Select_count = setting_Select_count + 1;
				if(setting_Select_count > 3)
				{
					setting_Select_count = 1;
				}
			}
			switch(setting_Select_count)
			{
				case 1: 
					if(key_val == 0x13 || key_val == 0x23)
					{
						if(key_val == 0x23) Delay_Ms(200);
						play_time[stanndby_Select_count].Hour = play_time[stanndby_Select_count].Hour + 1;
						if(play_time[stanndby_Select_count].Hour > 99)
							play_time[stanndby_Select_count].Hour = 0;
					}
					break;
				case 2: 
					if(key_val == 0x13 || key_val == 0x23)
					{
						if(key_val == 0x23) Delay_Ms(200);
						play_time[stanndby_Select_count].Min = play_time[stanndby_Select_count].Min + 1;
						if(play_time[stanndby_Select_count].Min > 59)
							play_time[stanndby_Select_count].Min = 0;						
					}
					break;
				case 3: 
					if(key_val == 0x13 || key_val == 0x23)
					{
						if(key_val == 0x23) Delay_Ms(200);
						play_time[stanndby_Select_count].Sec = play_time[stanndby_Select_count].Sec + 1;
						if(play_time[stanndby_Select_count].Sec > 59)
							play_time[stanndby_Select_count].Sec = 0;						
					}
					break;
			}	
			display(play_time[stanndby_Select_count], SETTING, setting_Select_count);
			if(key_val == 0x11 || key_val == 0x22)
			{
				UI_flag = 1;
				setting_Select_count = 1;
				save_data(play_time[stanndby_Select_count]);
			}
			else if(key_val == 0x14)
			{
				UI_flag = 3;
				setting_Select_count = 1;
				start_falg = 1;
			}
		}
		while(UI_flag == 3)
		{
			key_val = key_drive();
			if(start_falg == 1)
			{
				if(led_flag == 1){LED_ENANLE();}
				else if(led_flag == 0) {LED_DISANLE();}
				display(play_time[stanndby_Select_count], RUNNING, 0);
				TIM_Cmd(TIM3, ENABLE);
			}
			else if(start_falg == 0)
			{
				LED_DISANLE();
				display(play_time[stanndby_Select_count], PAUSE, 0);
				TIM_Cmd(TIM3, DISABLE);
				TIM3 -> CNT = 0;
				LED_DISANLE();
			}
			if(key_val == 0x14)
			{
				if(start_falg == 0)
					start_falg = 1;
				else if(start_falg == 1)
					start_falg = 0;
			}
			if(key_val == 0x24)
			{
				start_falg = 0;
				LED_DISANLE();
				CNT = 0;
				LED_CNT = 0;
				UI_flag = 1;
				TIM_Cmd(TIM3, DISABLE);
				TIM3 -> CNT = 0;
			}
		}
	}
}

/*
显示函数，
用来显示题目要求的各种功能
参数 第一个struct time_struct display_time用来显示传入的时间
	第二个uint8_t status 用来显示传入的状态信息
	第三个 uint8_t pos这个只在设置状态下有效，显示高亮的部分
*/
void display(struct time_struct display_time, uint8_t status, uint8_t pos)
{
	char buff[20]; //建立一个文字缓存区，sprintf()使用
	LCD_DisplayStringLine(Line2, (uint8_t *)"    No ");
	LCD_DisplayChar(Line2, 16*13, display_time.No);
	
	switch(status)
	{
		case STANNDBY: 
			sprintf(buff, "      %02d: %02d: %02d ", display_time.Hour, display_time.Min, display_time.Sec);
			LCD_DisplayStringLine(Line4, (uint8_t *)buff);
			LCD_DisplayStringLine(Line6, (uint8_t *)"       Standby");
			break;
		case SETTING: 
			sprintf(buff, "      %02d: %02d: %02d ", display_time.Hour, display_time.Min, display_time.Sec);
			set_display(Line4, (uint8_t *)buff ,pos);
			LCD_DisplayStringLine(Line6, (uint8_t *)"       Setting");
			break;
		case RUNNING: 
			sprintf(buff, "      %02d: %02d: %02d ", display_time.Hour, display_time.Min, display_time.Sec);
			LCD_DisplayStringLine(Line4, (uint8_t *)buff);		
			LCD_DisplayStringLine(Line6, (uint8_t *)"       Running"); 
			break;
		case PAUSE: 
			sprintf(buff, "      %02d: %02d: %02d ", display_time.Hour, display_time.Min, display_time.Sec);
			LCD_DisplayStringLine(Line4, (uint8_t *)buff);			
			LCD_DisplayStringLine(Line6, (uint8_t *)"       Pause"); 
			break;
		default : break;
	}
}

void set_display(uint8_t Line, uint8_t *ptr,uint8_t pos)
{
	uint32_t i = 0;
	uint16_t refcolumn = 319;//319;
	if(pos == 1) pos = 6; //这样就方便处理，只需要输入在哪里显示就行
	else if(pos == 2) pos = 10; //z在上层程序设计时就不需要考虑具体在那一个位置
	else if(pos == 3) pos = 14; //显示
	while ((*ptr != 0) && (i < 20))	 //	20
	{
		if((i == pos) || (i == (pos + 1))) //在符合条件的位置及其下一个位置显示
		{
			LCD_SetBackColor(White);
			LCD_SetTextColor(Blue);
		}
		else
		{
			LCD_SetBackColor(Blue);
			LCD_SetTextColor(White);
		}
		
		LCD_DisplayChar(Line, refcolumn, *ptr);
		refcolumn -= 16;
		ptr++;
		i++;
	}
}

void LED_gpio_cogfig(void)
{
	GPIO_InitTypeDef GPIO_structure;
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOD,ENABLE);
	
	GPIO_structure.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_structure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_structure.GPIO_Pin = GPIO_Pin_2;
	GPIO_Init(GPIOD, &GPIO_structure);
	GPIO_SetBits(GPIOD, GPIO_Pin_2);
	GPIO_Write(GPIOC, 0XFF00);
	GPIO_ResetBits(GPIOD, GPIO_Pin_2);
}

void Delay_Ms(u32 nTime)
{
	TimingDelay = nTime;
	while(TimingDelay != 0);	
}

/*--------------储存函数-----------------*/
void save_data(struct time_struct save_time)
{
	uint8_t addr = 0, i;
	switch(save_time.No) 
	{
		case '1': addr = 0x01; break;
		case '2': addr = 0x05; break;
		case '3': addr = 0x09; break;
		case '4': addr = 0x0d; break;
		case '5': addr = 0x11; break;
		default : break;
	}
	for(i = 0; i <4; i ++)
	{
		I2CStart();
		I2CSendByte(0xa0);
		while(!I2CWaitAck());
		I2CSendByte(addr + i);
		while(!I2CWaitAck());
		switch(i)
		{
			case 0:I2CSendByte(save_time.No);break;
			case 1:I2CSendByte(save_time.Hour);break;
			case 2:I2CSendByte(save_time.Min);break;
			case 3:I2CSendByte(save_time.Sec);break;
			default:break;
		}
		while(!I2CWaitAck());
		I2CStop();
		Delay_Ms(10);
	}
}

void read_data(struct time_struct *save_time, uint8_t No)
{
	uint8_t addr = 0, i;
	switch(No) 
	{
		case 1: addr = 0x01; break;
		case 2: addr = 0x05; break;
		case 3: addr = 0x09; break;
		case 4: addr = 0x0d; break;
		case 5: addr = 0x11; break;
		default : break;
	}
	for(i = 0; i < 4; i ++)
	{
		I2CStart();
		I2CSendByte(0xa0);
		while(!I2CWaitAck());
		I2CSendByte(addr+i);
		while(!I2CWaitAck());
		I2CStart();
		I2CSendByte(0xa1);
		while(!I2CWaitAck());
		switch(i)
		{
			case 0: save_time->No   = I2CReceiveByte(); break;
			case 1: save_time->Hour = I2CReceiveByte(); break;
			case 2: save_time->Min  = I2CReceiveByte(); break;
			case 3: save_time->Sec  = I2CReceiveByte(); break;
			default : break;
		}
		I2CSendAck();
		I2CStop();
	}	
}
/*----------------结束-----------------*/


void TIM4_IRQHandler()
{
	if(TIM_GetITStatus(TIM4, TIM_IT_Update) != RESET)
	{
		 if(start_falg)
		 {
			CNT ++;
			LED_CNT ++;
			if(LED_CNT > 500)
			{
				if(led_flag == 1)
					led_flag = 0;
				else if(led_flag == 0)
					led_flag = 1;
				LED_CNT = 0;
			}
								
			if(CNT >= 999)
			{
				play_time[stanndby_Select_count].Sec = play_time[stanndby_Select_count].Sec - 1;
				if(play_time[stanndby_Select_count].Sec < 0)
				{
					play_time[stanndby_Select_count].Min = play_time[stanndby_Select_count].Min - 1;
					if(play_time[stanndby_Select_count].Min < 0)
					{
						play_time[stanndby_Select_count].Hour = play_time[stanndby_Select_count].Hour - 1;
						if(play_time[stanndby_Select_count].Hour < 0)
						{
							start_falg = 0;
							play_time[stanndby_Select_count].Hour = 0;
							play_time[stanndby_Select_count].Min = 0;
							play_time[stanndby_Select_count].Sec = 0;	
						}
						else 
						{
							play_time[stanndby_Select_count].Min = 59;
							play_time[stanndby_Select_count].Sec = 59;
						}
					}
					else
					{
						play_time[stanndby_Select_count].Sec = 59;	
					}
				}
				CNT = 0;
			}
		 }
		 key_scan();
		 TIM_ClearITPendingBit(TIM4, TIM_IT_Update);
	}
}


