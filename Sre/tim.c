#include "tim.h"

void TIM4_Config(void)
{
	TIM_TimeBaseInitTypeDef TIM_structure;
	NVIC_InitTypeDef NVIC_structure;
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM4, ENABLE);
	
	TIM_structure.TIM_ClockDivision = TIM_CKD_DIV1;
	TIM_structure.TIM_RepetitionCounter = 0;
	TIM_structure.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_structure.TIM_Prescaler = 72 -1;
	TIM_structure.TIM_Period = 1000 - 1;
	TIM_TimeBaseInit(TIM4, &TIM_structure);
	
	NVIC_structure.NVIC_IRQChannel = TIM4_IRQn;
	NVIC_structure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_structure.NVIC_IRQChannelSubPriority = 3;
	NVIC_structure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_structure);
	TIM_ClearFlag(TIM4, TIM_FLAG_Update);
	TIM_ITConfig(TIM4, TIM_IT_Update, ENABLE);
	TIM_Cmd(TIM4, ENABLE);
}


static void PWM_gpio_config()
{
	GPIO_InitTypeDef GPIO_structure;
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA,ENABLE);
	
	GPIO_structure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_structure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_structure.GPIO_Pin = GPIO_Pin_6;
	GPIO_Init(GPIOA, &GPIO_structure);
}
void TIM3_Config(void)
{
	TIM_TimeBaseInitTypeDef TIM_structure;
	TIM_OCInitTypeDef pwm_structure;
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE);
	
	TIM_structure.TIM_ClockDivision = TIM_CKD_DIV1;
	TIM_structure.TIM_RepetitionCounter = 0;
	TIM_structure.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_structure.TIM_Prescaler = 72 -1;
	TIM_structure.TIM_Period = 1000 - 1;
	TIM_TimeBaseInit(TIM3, &TIM_structure);	
	
	pwm_structure.TIM_OCMode = TIM_OCMode_PWM1;
	pwm_structure.TIM_OutputState = TIM_OutputState_Enable;
	pwm_structure.TIM_OCPolarity = TIM_OCPolarity_Low;
	TIM_OC1Init(TIM3, &pwm_structure);
	
	TIM_OC1PreloadConfig(TIM3, TIM_OCPreload_Enable);
	TIM_SetCompare1(TIM3, 200);
	PWM_gpio_config();
	
	TIM_Cmd(TIM3, DISABLE);
	
}
